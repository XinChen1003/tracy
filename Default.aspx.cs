﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Net;
using System.Net.Mail;
using System.IO;
using System.Security.Cryptography;

namespace Portfolio
{
    public partial class _Default : Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(Request.QueryString.Get("AjaxRequest")) && Request.QueryString.Get("AjaxRequest") == "true")
            {
                HandleAjaxRequest();
            }

        }

        //[System.Web.Services.WebMethod]
        //public static bool SaveData(string name, string email, string phone, string message)
        //{

        //    string cs = ConfigurationManager.ConnectionStrings["PortfolioConnection"].ConnectionString;
        //    using (SqlConnection con = new SqlConnection(cs))
        //    {
        //        SqlCommand cmd = new SqlCommand("INSERT dbo.contact_inform(Name ,Email, Phone_Number , Message_Left) VALUES ('" + name + "', '" + email + "' ,'" + phone + "' ,'" + message + "')", con);
        //        SqlDataAdapter adapter = new SqlDataAdapter(cmd);
        //        SqlDataReader reader;
        //        con.Open();
        //        reader = cmd.ExecuteReader();
        //        con.Close();
        //    }
        //    return true;
        //}
        protected virtual void HandleAjaxRequest()
        {
            string _name = Request.Form["name"];
            string _email = Request.Form["email"];
            string _phone = Request.Form["phone"];
            string _message = Request.Form["message"];

            string cs = ConfigurationManager.ConnectionStrings["PortfolioConnection"].ConnectionString;
            using (SqlConnection con = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand("INSERT dbo.contact_inform(Name ,Email, Phone_Number , Message_Left) VALUES ('" + _name + "', '" + _email + "' ,'" + _phone + "' ,'" + _message + "')", con);
                SqlDataReader reader;
                con.Open();
                reader = cmd.ExecuteReader();
                con.Close();
            }
            string firstName = _name;
            if(_name.IndexOf(' ') != -1)
            {
                firstName = _name.Split(' ')[0];
            }
            SendEmails(_email, firstName);

        }
        public virtual void SendEmails(string contactEmail, string name)
        {
            try
            {
                string userEmail = ConfigurationManager.AppSettings["mail"];
                string userPass = ConfigurationManager.AppSettings["pass"];
                string myDecryted = new AESKeyUtility().Decrypt(userPass);
                SmtpClient client = new SmtpClient("smtp.gmail.com");
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential(userEmail, myDecryted);
                client.Port = 587;
                client.EnableSsl = true;
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(userEmail, "XC@tracy.jadenweb.com");
                mail.To.Add(contactEmail);
                string Subject = "Tracy.jadenweb.com";
                string Body = "Hi "+name+",@@Thank you for reaching out! @Your message has been received and I will get back to you as soon as possible!".Replace("@", Environment.NewLine);
                string Footer = "@@Best Regards,@Xin Chen".Replace("@", Environment.NewLine);
                mail.Subject = Subject;
                mail.Body = Body + Footer;
                client.Send(mail);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in sendEmail:" + ex.Message);
            }
        }
    }

}