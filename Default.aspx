﻿<%@ Page Title="Home Page" Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Portfolio._Default" %>

    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="xinchen">
        <title>Xin Chen</title>
        <!--Bootstrap 4.0.0-beta.2 CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb"
            crossorigin="anonymous">
        <!-- Magnific-popup CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
        <!--Custom styles for this portfolio -->
        <link rel="stylesheet" type="text/css" href="css/base.css">
        <!--Custom fonts for this portfolio -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    </head>

    <body id="page-top">
        <!-- Navigation -->
        <nav class="navbar navbar-expand-lg navbar-light fixed-top bg-dark" id="mainNav">
            <div class="container">
                <a class="navbar-brand js-scroll-trigger" href="#header">XC</a>
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                    aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                Menu
                <i class="fa fa-bars"></i>
            </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="#portfolio">Portfolio</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="#about">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Header -->
        <header class="masthead text-center" id="header">
            <div class="container">
                <img class="img-fluid mb-5 d-block mx-auto" src="img/me.png" alt="" style="width: 250px; height: auto;">
                <h1 class="text-uppercase mb-0">Xin Chen</h1>
                <hr class="star-light">
                <h2 class="font-weight-light mb-0">Web Developer - Graphic Artist - User Experience Designer</h2>
            </div>
        </header>
        <!-- Portfolio -->
        <section class="portfolio bg-light" id="portfolio">
            <div class="container">
                <h2 class="text-center text-uppercase text-dark mb-0">Portfolio</h2>
                <hr class="star-dark mb-5">
                <div class="row">
                    <div class="col-md-12 col-lg-6">
                        <a class="portfolio-item d-block mx-auto" href="http://tracy.jadenweb.com/projects/simonGame/" target="_blank">
                            <div class="portfolio-item-caption d-flex position-absolute h-100 w-100">
                                <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">
                                    <i class="fa fa-search-plus fa-3x"></i>
                                </div>
                            </div>
                            <img class="img-fluid" src="img/simon_game.png" alt="simon_game" style="width: 560px; height: auto" />
                        </a>
                        <article class="portfolio-item d-block mx-auto">
                            <h4 class="text-secondary text-uppercase text-center mb-0">Simon Game</h4>
                            <p class="mb-5">Building a Simon Game using JavaScript and pure HTML/CSS for the interface.</p>
                        </article>
                    </div>
                    <div class="col-md-12 col-lg-6">
                        <a class="portfolio-item d-block mx-auto" href="http://tracy.jadenweb.com/projects/tictactoegame/" target="_blank">
                            <div class="portfolio-item-caption d-flex position-absolute h-100 w-100">
                                <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">
                                    <i class="fa fa-search-plus fa-3x"></i>
                                </div>
                            </div>
                            <img class="img-fluid" src="img/tic_tac_toe_game.png" alt="tic_tac_toe_game" style="width: 560px; height: auto;">
                        </a>
                        <article class="portfolio-item d-block mx-auto">
                            <h4 class="text-secondary text-uppercase text-center mb-0">Tic Tac Toe Game</h4>
                            <p class="mb-5">A Javascript game of Tic Tac Toe. This project is to practice my Javascript. There are lots of
                                interaction. I had lots of fun building the unbeatable AI for the computer.</p>
                        </article>
                    </div>
                    <div class="col-md-12 col-lg-6">
                        <a class="portfolio-item d-block mx-auto" href="http://tracy.jadenweb.com/projects/localWeather/" target="_blank">
                            <div class="portfolio-item-caption d-flex position-absolute h-100 w-100">
                                <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">
                                    <i class="fa fa-search-plus fa-3x"></i>
                                </div>
                            </div>
                            <img class="img-fluid" src="img/local_weather.png" alt="local_weather" style="width: 560px; height: auto;">
                        </a>
                        <article class="portfolio-item d-block mx-auto">
                            <h4 class="text-secondary text-uppercase text-center mb-0">Local Weather</h4>
                            <p class="mb-5">Built a weather application to display local weather using Ajax to manipulate darkSky API in
                                real time</p>
                        </article>
                    </div>
                    <div class="col-md-12 col-lg-6">
                        <a class="portfolio-item d-block mx-auto" href="http://tracy.jadenweb.com/projects/twitchStreamers/" target="_blank">
                            <div class="portfolio-item-caption d-flex position-absolute h-100 w-100">
                                <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">
                                    <i class="fa fa-search-plus fa-3x"></i>
                                </div>
                            </div>
                            <img class="img-fluid" src="img/twitch_streamers.png" alt="twitch_streamers" style="width: 560px; height: auto;">
                        </a>
                        <article class="portfolio-item d-block mx-auto">
                            <h4 class="text-secondary text-uppercase text-center mb-0">Twitch Streamers</h4>
                            <p class="mb-5">This is a advanced project for Free Code Camp. The purpose of this project is to learn about
                                using the data from API service, and learn about async, callback in Javascript.</p>
                        </article>
                    </div>
                    <div class="col-md-12 col-lg-6">
                        <a class="portfolio-item d-block mx-auto" href="http://tracy.jadenweb.com/projects/wiki/" target="_blank">
                            <div class="portfolio-item-caption d-flex position-absolute h-100 w-100">
                                <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">
                                    <i class="fa fa-search-plus fa-3x"></i>
                                </div>
                            </div>
                            <img class="img-fluid" src="img/wikipedia.png" alt="wikipedia" style="width: 560px; height: auto;">
                        </a>
                        <article class="portfolio-item d-block mx-auto">
                            <h4 class="text-secondary text-uppercase text-center mb-0">Wikipedia Viewer</h4>
                            <p class="mb-5">A Wikipedia Viewer/Searcher to find any thing from Wikipedia database by consuming Wikipedia
                                API.</p>
                        </article>
                    </div>
                    <div class="col-md-12 col-lg-6">
                        <a class="portfolio-item d-block mx-auto" href="http://tracy.jadenweb.com/projects/calculator/" target="_blank">
                            <div class="portfolio-item-caption d-flex position-absolute h-100 w-100">
                                <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">
                                    <i class="fa fa-search-plus fa-3x"></i>
                                </div>
                            </div>
                            <img class="img-fluid" src="img/calculator.png" alt="calculator" style="width: 560px; height: auto;">
                        </a>
                        <article class="portfolio-item d-block mx-auto">
                            <h4 class="text-secondary text-uppercase text-center mb-0">JavaScript Calculator</h4>
                            <p class="mb-5">This was a project that build a fully functional calcultor. Good practice of using JavaScript,
                                HTML5/CSS3 and jQuery.</p>
                        </article>
                    </div>
                </div>
            </div>
        </section>
        <!-- About -->
        <section class="success" id="about">
            <div class="container">
                <h2 class="text-center">About</h2>
                <hr class="star-light">
                <div class="row">
                    <div class="col-lg-4 ml-auto">
                        <p>Hello, my name is Xin. I am an Full Stack Web Developer living in San Mateo, CA. My skills includ
                            HTML/CSS, JavaScript, Bootstrap, C#, ASP.NET, SQL.</p>
                    </div>
                    <div class="col-lg-4 mr-auto">
                        <p>I have experience on both frontend and backend development. I am looking for an opportunity that
                            would help me grow my skills and experience. If you want to work a project together, feel free
                            to contact me.</p>
                    </div>
                </div>
            </div>
        </section>
        <!-- Contact -->
        <section class="bg-light" id="contact">
            <div class="container">
                <h2 class="text-center text-uppercase text-dark mb-0">Contact Me</h2>
                <hr class="star-dark mb-5">
                <div class="row">
                    <div class="col-lg-8 mx-auto">
                        <form name="sentMessage" id="contactForm" novalidate="novalidate" runat="server">
                            <div class="control-group">
                                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                    <label>Name</label>
                                    <input class="form-control" id="myName" type="text" placeholder="Name" required="required" data-validation-required-message="Please enter your name."
                                        runat="server">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                    <label>Email Address</label>
                                    <input class="form-control" id="myEmail" type="email" placeholder="Email Address" required="required" data-validation-required-message="Please enter your email address."
                                        runat="server">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                    <label>Phone Number</label>
                                    <input class="form-control" id="myPhone" type="tel" placeholder="Phone Number" required="required" data-validation-required-message="Please enter your phone number."
                                        runat="server">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                    <label>Message</label>
                                    <textarea class="form-control" id="MyMessage" rows="5" placeholder="Message" required="required" data-validation-required-message="Please enter a message."
                                        runat="server"></textarea>
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <br>
                            <div id="success"></div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-xl" id="sendMessageButton" runat="server">Send</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

        <!-- Footer -->
        <footer class="footer text-center bg-dark text-light">
            <div class="footer-above">
                <div class="container">
                    <div class="row">
                        <div class="footer-col col-lg-12 mx-auto">
                            <h4 class="text-uppercase mb-4">Social medias</h4>
                            <ul class="list-inline">
                                <li class="list-inline-item mx-5">
                                    <a class="btn-social btn-outline" href="https://bitbucket.org/XinChen1003/" target="_blank"><i class="fa fa-bitbucket" aria-hidden="true"></i></a>
                                </li>
                                <li class="list-inline-item mx-5">
                                    <a class="btn-social btn-outline" href="https://www.linkedin.com/in/xin-chen-aaa0aa77/" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                                </li>
                                <li class="list-inline-item mx-5">
                                    <a class="btn-social btn-outline" href="https://github.com/Tracychen89" target="_blank"><i class="fa fa-github" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                            <div class="copyright"> Copyright &copy; Xin Chen 2017</div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Scroll to top button -->
        <div class="scroll-to-top d-lg-none position-fixed">
            <a class="d-block text-center text-white js-scroll-trigger rounded" href="#page-top">
        <i class="fa fa-chevron-up"></i>
      </a>
        </div>
        <!-- JS -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="Vendor/jqBootstrapValidation.js"></script>
        <script src="Scripts/contact_me.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh"
            crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ"
            crossorigin="anonymous"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
        <script src="Scripts/portfolio.js"></script>

    </body>

    </html>