var myArray = ["ESL_SC2", "OgamingSC2", "cretetion", "freecodecamp", "storbeck", "habathcx", "RobotCaleb", "noobs2ninjas"];
function getInfor() {
    myArray.forEach(function (item) {

        var api = "https://wind-bow.gomix.me/twitch-api/streams/" + item + "?callback=?";
        console.log(item);
        $.getJSON(api, function (data) {

            var game, color;
            if (data.stream === null) {
                color = "offline";
                game = "Offline";
            }
            else if (data.stream == undefined) {
                color = "offline";
                game = "Account Closed";
            }
            else {
                color = "lime";
                game = data.stream.game;
            };
            var apiChan = "https://wind-bow.gomix.me/twitch-api/channels/" + item + "?callback=?";

            $.getJSON(apiChan, function (dataC) {
                var logo = dataC.logo != null ? dataC.logo : "https://dummyimage.com/50x50/ecf0e7/5c5457.jpg&text=0x3F";
                var name = dataC.display_name != null ? dataC.display_name : item;
                console.log(item);
                var html = "<div class='row " + color + "'><div class='col-xs-2 col-sm-3 col-md-3'><img src=" + logo + " class='logo'></div><div class='col-xs-10 col-sm-3 col-md-3 status'><a href='" + dataC.url + "' target='_blank' >" + name + "</a></div><div class='col-xs-10 col-sm-6 col-md-6 game'>" + game + "</div> </div>"
                color === "lime" ? $("#result").prepend(html) : $("#result").append(html);
            })
        })
    })
}
$(document).ready(function () {
    getInfor();
    $("#onSelector").click(function () {
        $("#result .lime").css("display", "block");
        $("#result .offline").css("display", "none");
    })
    $("#offSelector").click(function () {
        $("#result .lime").css("display", "none");
        $("#result .offline").css("display", "block");

    })
    $("#allSelector").click(function () {

        $("#result .row").css("display", "block");
    })

})