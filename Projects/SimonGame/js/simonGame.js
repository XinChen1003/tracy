$(document).ready(function () {

    // check whether the browser support audio API or not
    var AudioContext = window.AudioContext || window.webkitAudioContext;
    if (!AudioContext) {
        //audio API doesn't work
        alert("Sorry, your browser doesn't support the Web Audio API.");
    }
    else {
        //audio API works
        var audioCtx = new AudioContext;

        //create err sounds
        var errOsc = audioCtx.createOscillator();
        errOsc.style = "triangle";
        errOsc.frequency.value = 120;
        errOsc.start(0);
        var errGainNode = audioCtx.createGain();
        errOsc.connect(errGainNode);
        errGainNode.gain.value = 0;
        errGainNode.connect(audioCtx.destination);

        //create a gradual linear change in volume
        var ramp = 0.05;
        var val = 0.5;

        //create a array with four oscillators element
        var frequencies = [329.63, 261.63, 220, 164.81];
        var oscillators = frequencies.map(function (frq) {
            var osc = audioCtx.createOscillator();
            osc.type = 'sine';
            osc.frequency.value = frq;
            osc.start(0.0);
            return osc;
        })
        //create a array with four gainNodes element
        var gainNodes = oscillators.map(function (osc) {
            var gainNode = audioCtx.createGain();
            osc.connect(gainNode);
            gainNode.connect(audioCtx.destination);
            gainNode.gain.value = 0;
            return gainNode;
        })

        // functions about sounds
        function playGoodTone(num) {
            // debugger
            //change volume from 0 to 0.5 at 0.05s
            gainNodes[num].gain.linearRampToValueAtTime(val, audioCtx.currentTime + ramp);
            gameStatus.currPush = $("#" + num);
            gameStatus.currPush.addClass("light");
        }
        function stopGoodTone() {
            // debugger
            if (gameStatus.currPush) {
                gameStatus.currPush.removeClass("light");
                gainNodes.forEach(function (g) {
                    g.gain.linearRampToValueAtTime(0, audioCtx.currentTime + ramp);
                })
                gameStatus.currPush = undefined;
                gameStatus.currOsc = undefined;
            }
        }
        function playErrTone() {
            debugger
            flashMessage("!!", 2)
            errGainNode.gain.linearRampToValueAtTime(val, audioCtx.currentTime + ramp);
        }

        function stopErrTone() {
            errGainNode.gain.linearRampToValueAtTime(0, audioCtx.currentTime + ramp);
        }
        function notifyError() {
            debugger
            gameStatus.lock = true;
            $(".btns").removeClass("clickable").addClass("unclickable");
            playErrTone();
            gameStatus.toHndTimeout = setTimeout(stopErrTone, 2000);
           
            if (gameStatus.strict) {
                gameStatus.toHndTimeout = setTimeout(gameStart, 3000);
            }
            else {
                gameStatus.toHndTimeout = setTimeout(playSequence, 3000);
            }


        }
        function notifyWin() {
            var cnt = 0;
            var last = gameStatus.lastPush;
            gameStatus.seqHndl = setInterval(function () {
                debugger
                playGoodTone(last);
                gameStatus.toHndTimeout = setTimeout(stopGoodTone, 80);
                cnt++;
                if (cnt === 8) {
                    clearInterval(gameStatus.seqHndl);
                }
            }, 160);
            flashMessage('**', 2);

        }

        var gameStatus = {};
        gameStatus.reset = function () {
            this.init;
            this.strict = false;
        }
        gameStatus.init = function () {
            this.count = 0;
            this.playerStep = 0;
            this.lock = true;
            this.currPush = undefined;
            this.currOsc = undefined;
            this.sequence = [];
            this.playerSeq = [];
            this.lastPush;

        }
        function addStep() {
            // debugger
            stopGoodTone();
            gameStatus.sequence.push(Math.floor(Math.random() * 4));
            gameStatus.toHndTimeout = setTimeout(playSequence, 1000);
            gameStatus.count++;
            // display();
        }
        function display() {
            // debugger
            if (gameStatus.count < 10) {
                $("#count").text("0" + gameStatus.count);
            }
            else {
                $("#count").text(gameStatus.count);
            }
        }
        function playSequence() {
            var i = 0;
            gameStatus.playerStep = 0;
            gameStatus.toHndInterval = setInterval(function () {
                display();
                // debugger
                gameStatus.lock = true;
                playGoodTone(gameStatus.sequence[i]);
                gameStatus.toHndTimeout = setTimeout(stopGoodTone, 300);
                i++;
                if (i == gameStatus.sequence.length) {
                    clearInterval(gameStatus.toHndInterval);
                    gameStatus.lock = false;
                    $(".btns").removeClass("unclickable").addClass("clickable");
                    gameStatus.toHndTimeout = setTimeout(notifyError, 5000);
                }

            }, 800);
        }
        function pushBtns(btn) {
            // pushBtns function works only when gameStatus.lock is false which happen after playSequence function be called
            if (!gameStatus.lock) {
                debugger
                clearTimeout(gameStatus.toHndTimeout);
                var myId = btn.attr("id");
                if (myId == gameStatus.sequence[gameStatus.playerStep]) {
                    debugger
                    gameStatus.lastPush = myId;
                    playGoodTone(myId);
                    setTimeout(stopGoodTone, 80);
                    gameStatus.playerStep++;
                    //player doesn't play enough steps on time
                    if (gameStatus.playerStep < gameStatus.sequence.length) {
                        gameStatus.toHndTimeout = setTimeout(notifyError, 1000);
                    }
                    //player wins when player plays 20 steps 
                    else if (gameStatus.playerStep == 5) {
                        gameStatus.toHndTimeout = setTimeout(notifyWin, 1000);
                    }
                    //player finish this round and game continue
                    else {
                        $(".btns").removeClass("clickable").addClass("unclickable");
                        setTimeout(addStep, 500);
                    }
                }
                else {
                    $(".btns").removeClass("clickable").addClass("unclickable");
                    notifyError(btn);
                }

            }
        }
        function gameStart() {
            // debugger
            resetTimer();
            gameStatus.init();
            flashMessage("--", 2);
            addStep();
        }
        function resetTimer() {
            clearTimeout(gameStatus.toHndFl);
            clearTimeout(gameStatus.toHndTimeout);
            clearInterval(gameStatus.flHndl);
            clearInterval(gameStatus.toHndInterval);
            clearInterval(gameStatus.seqHndl);
        }
        function flashMessage(msg, times) {
            $("#count").text(msg);
            gameStatus.flHndl = setInterval(function () {
                debugger
                $("#count").removeClass("led-on").addClass("led-off");
                gameStatus.toHndFl = setTimeout(function () {
                    $("#count").removeClass("led-off").addClass("led-on");;
                }, 250);
                if (--times == 0) {
                    clearInterval(gameStatus.flHndl);
                };
            }, 500)

        }
        function strictGame() {
            debugger
            $("#strictLight").toggleClass("led-on");
            gameStatus.strict = !gameStatus.strict;
        }
        $("#switch").on("change", function () {
            if ($("#switch").prop("checked")) {
                debugger
                gameStatus.reset();
                $("#count").removeClass("led-off").addClass("led-on");
                $(".btns").removeClass("unclickable").addClass("clickable");
                $("#start").removeClass("unclickable").addClass("clickable");
                $("#strict").removeClass("unclickable").addClass("clickable");
            }
            else {
                debugger
                if (gameStatus.strict) {
                    $("#strictLight").removeClass("led-on");
                }
                gameStatus.reset();
                resetTimer();
                stopGoodTone();
                stopErrTone();
                $("#count").text("--");
                $("#count").removeClass("led-on").addClass("led-off");
                $(".btns").removeClass("clickable").addClass("unclickable");
                $("#start").removeClass("clickable").addClass("unclickable");
                $("#strict").removeClass("clickable").addClass("unclickable");
            }

        })
        $(".btns").mousedown(function () {
            pushBtns($(this));
        })
        $("#start").click(gameStart);
        $("#strict").click(strictGame);



    }



})