$(document).ready(function () {
    $("#icon").click(function () {
        $("#icon").css("display", "none");
        $("#myInput").css("display", "inline");
        var btn = document.createElement("BUTTON");
        var text = document.createTextNode("Search");
        btn.id = "myBtn";
        btn.appendChild(text);
        var mydiv = document.getElementById("searchContainer");
        mydiv.appendChild(btn);

    })
    $(document).on("click", "#myBtn", function () {
        var title = $("#myInput").val();
        var api = "https://en.wikipedia.org/w/api.php?action=opensearch&search=" + title + "&format=json&callback=?";
        $.getJSON(api, function (json) {
            $("#result").empty();
            for (var i = 0; i < json[1].length; i++) {
                $("#result").append("<li><div class=re-header><a href=" + json[3][i] + " target=_blank><i class= 'fa fa-external-link' aria-hidden= true></i></a><button class= accordion>" + json[1][i] + "</button></div><div class=re-body><p>" + json[2][i] + "</p></div></li>");
                $(".container").css("padding-top", "5%");
            }
            var acc = document.getElementsByClassName("accordion");
            for (var i = 0; i < acc.length; i++) {
                acc[i].onclick = function () {
                    debugger;
                    this.classList.toggle("active");
                    var rebody = this.parentElement.nextSibling;
                    if (rebody.style.maxHeight !== "") {
                        rebody.style.maxHeight = ""
                    }
                    else {
                        rebody.style.maxHeight = rebody.scrollHeight + "px";
                    }
                }
            }
        });
    })
})

