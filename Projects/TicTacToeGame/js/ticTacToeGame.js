$(document).ready(function () {
    var winPaths = [[1, 2, 3], [4, 5, 6], [7, 8, 9], [1, 4, 7], [1, 5, 9], [2, 5, 8], [3, 6, 9], [3, 5, 7]];
    var isX = true;
    var firstGame = true;
    var scoreX = [];
    var scoreO = [];
    var winPath = [];
    var winCount1 = 0;
    var winCount2 = 0;
    var count = 0;
    var firstPlayer, player1, player2;
    var p1Turn;
    var reset = false;
    var board = { 1: "", 2: "", 3: "", 4: "", 5: "", 6: "", 7: "", 8: "", 9: "" };
    var onePlayer = false;
    var computer;

    $("#one-player-btn").click(function () {
        onePlayer = true;
        $("#game-choice").toggleClass("hidden");
        $("#symbol-choice").toggleClass("hidden");
        $("#two-player-element").css("display", "none");
        $(".player2").text("computer");
    })

    $("#two-player-btn").click(function () {
        $("#game-choice").toggleClass("hidden");
        $("#symbol-choice").toggleClass("hidden");
        $("#two-player-element").css("display", "inline");
        $(".player2").text("player2");
    })

    $("#backBtn").click(function () {
        $("#game-choice").toggleClass("hidden");
        $("#symbol-choice").toggleClass("hidden");
    })

    $("#symbol-x-btn").click(function () {
        debugger
        $("#symbol-choice").toggleClass("hidden");
        $(".game-top").toggleClass("disappear");
        player1 = "X";
        player2 = "O";
        if (onePlayer) {
            computer = player2;
        }
        if (!reset) {
            drawBorder();
            resetSquare();
        } else {
            $("#myCanvas").toggleClass("hidden");
            $("#boxs").toggleClass("hidden");
        }
        whoStarts();
    })
    $("#symbol-o-btn").click(function () {
        debugger
        $("#symbol-choice").toggleClass("hidden");
        $(".game-top").toggleClass("disappear");
        player1 = "O";
        player2 = "X";
        if (onePlayer) {
            computer = player2;
        }
        if (!reset) {
            drawBorder();
            resetSquare();
        } else {
            $("#myCanvas").toggleClass("hidden");
            $("#boxs").toggleClass("hidden");
        }
        whoStarts();
    })
    //can't use $("#boxs div").click(function () {} because #boxs divs elements are created after the document.ready
    // but you can delegate the click to a parent, in this instance document
    $(document).on('click', '#boxs div', function () {
        var symbol = p1Turn ? player1 : player2;
        var value = $(this).data("value");
        if ($.inArray(value, scoreX) == -1 && $.inArray(value, scoreO) == -1) {
            $(this).text(symbol);
            count++;

            if (symbol === "X") {
                scoreX.push(value);
            }
            else {
                scoreO.push(value);
            }
            resultDetermin();
            if (onePlayer) {
                board[value] = symbol;
                computerMove();
            }

        }
    });
    $("#resetBtn").click(resetGame);
    function drawBorder() {
        var c = document.getElementById("myCanvas");
        var ctx = c.getContext("2d");
        ctx.lineWidth = 1;
        ctx.strokeStyle = "white"

        ctx.moveTo(130, 0);
        ctx.lineTo(130, 390);
        ctx.stroke();

        ctx.moveTo(260, 0);
        ctx.lineTo(260, 390);
        ctx.stroke();

        ctx.moveTo(0, 130);
        ctx.lineTo(390, 130);
        ctx.stroke();

        ctx.moveTo(0, 260);
        ctx.lineTo(390, 260);
        ctx.stroke();
    }

    function resetSquare() {
        $("#boxs").html("");
        for (var i = 1; i <= 9; i++) {
            var box = "<div id='box" + i + "' data-value='" + i + "'></div>";
            $("#boxs").append(box);
        }
    }
    function resetGame() {
        debugger
        reset = true;
        if (winPath.length > 0) {
            for (var i = 0; i < winPath[0].length; i++) {
                var number = 0;
                number = winPath[0][i];
                $("#boxs div:nth-child(" + number + ")").css("background-color", "");
            }
        }
        $("#player1-turn").css("top", "0px", "z-index", "0");
        $("#player2-turn").css("top", "0px", "z-index", "0");
        $("#player1-score").html(0);
        $("#player2-score").html(0);
        $("#myCanvas").addClass("hidden");
        $("#boxs").addClass("hidden");
        $("#result").addClass("hidden");
        $("#boxs div").empty();
        $(".game-top").addClass("disappear");
        $("#game-choice").toggleClass("hidden");
        $("#symbol-choice").addClass("hidden");
        onePlayer = false;
        isX = true;
        firstGame = true;
        scoreX = [];
        scoreO = [];
        winPath = [];
        winCount1 = 0;
        winCount2 = 0;
        count = 0;
        board = { 1: "", 2: "", 3: "", 4: "", 5: "", 6: "", 7: "", 8: "", 9: "" };
    }

    function whoStarts() {
        debugger
        // only the first game's first player need to be decide randomly; 
        //after that, switch the first player according to last game's first player
        if (firstGame) {
            firstPlayer = Math.floor(Math.random() * 2 + 1) === 1 ? player1 : player2;
        }
        else {
            $("#result").toggleClass("hidden");
            $("#myCanvas").toggleClass("hidden");
            $("#boxs div").empty();
            $("#boxs").toggleClass("hidden");
        }
        //reset p1Turn according to first player
        p1Turn = firstPlayer === player1 ? true : false;
        if (p1Turn) {
            $("#player1-turn").css("top", "-50px", "z-index", "0");
        }
        else {
            $("#player2-turn").css("top", "-50px", "z-index", "0");
            if (onePlayer) {
                computerMove();
            }
        }
    }

    function whoWins(winPaths, score) {
        var arr = winPaths.filter(function (obj) {
            for (var j = 0; j < 3; j++) {
                if (!score.includes(obj[j])) {
                    return false;
                }
            }
            return true;
        })
        if (arr.length > 0) {
            winPath = arr;
            return true;
        }
        else {
            return false;
        }
    }
    function resultDetermin() {
        debugger
        if (whoWins(winPaths, scoreX)) {
            for (var i = 0; i < winPath[0].length; i++) {
                var number = 0;
                number = winPath[0][i];
                $("#player1-turn").css("top", "0px", "z-index", "0");
                $("#player2-turn").css("top", "0px", "z-index", "0");
                $("#boxs div:nth-child(" + number + ")").css("background-color", "#01267A");

            }
            setTimeout(function () {
                for (var i = 0; i < winPath[0].length; i++) {
                    var number = 0;
                    number = winPath[0][i];
                    $("#boxs div:nth-child(" + number + ")").css("background-color", "");

                }
                $("#myCanvas").toggleClass("hidden");
                $("#boxs").toggleClass("hidden");
                if (player1 === "X") {
                    winCount1++;
                    $("#player1-score").html(winCount1);
                    if (onePlayer) {
                        $("#result").html("You win!!!");
                    }
                    else {
                        $("#result").html("Player 1 wins!!");
                    }
                    $("#result").toggleClass("hidden");
                }
                else {
                    winCount2++
                    if (onePlayer) {
                        $("#result").html("Sorry, you lose...");
                    }
                    else {
                        $("#result").html("Player 2 wins!!");
                    }
                    $("#result").toggleClass("hidden");
                    $("#player2-score").html(winCount2);
                }
                $('#resetBtn').prop('disabled', true);
                setTimeout(moreGames, 3000);

            }, 1000);
        }
        else if (whoWins(winPaths, scoreO)) {
            for (var i = 0; i < winPath[0].length; i++) {
                var number = 0;
                number = winPath[0][i];
                $("#player1-turn").css("top", "0px", "z-index", "0");
                $("#player2-turn").css("top", "0px", "z-index", "0");
                $("#boxs div:nth-child(" + number + ")").css("background-color", "#01267A");

            }
            setTimeout(function () {
                for (var i = 0; i < winPath[0].length; i++) {
                    var number = 0;
                    number = winPath[0][i];
                    $("#boxs div:nth-child(" + number + ")").css("background-color", "");

                }
                $("#myCanvas").addClass("hidden");
                $("#boxs").addClass("hidden");
                if (player1 === "O") {
                    winCount1++;
                    $("#player1-score").html(winCount1);
                    if (onePlayer) {
                        $("#result").html("You win!!!");
                    }
                    else {
                        $("#result").html("Player 1 wins!!");
                    }
                    $("#result").toggleClass("hidden");
                }
                else {
                    winCount2++;
                    $("#player2-score").html(winCount2);
                    if (onePlayer) {
                        $("#result").html("Sorry, you lose...");
                    }
                    else {
                        $("#result").html("Player 2 wins!!");
                    }
                    $("#result").toggleClass("hidden");
                }
                debugger
                $('#resetBtn').prop('disabled', true);
                setTimeout(moreGames, 4000);

            }, 3000);

        }
        else if (count == 9) {
            $("#myCanvas").addClass("hidden");
            $("#boxs").addClass("hidden");
            $("#result").html("This is a tie!!");
            $("#result").toggleClass("hidden");
            $("#player1-turn").css("top", "0px", "z-index", "0");
            $("#player2-turn").css("top", "0px", "z-index", "0");
            $('#resetBtn').click(function () {
                $(this).data('clicked', true);
            });
            $('#resetBtn').prop('disabled', true);
            setTimeout(moreGames, 4000);

        }
        else {
            if (p1Turn) {
                $("#player2-turn").css("top", "-50px", "z-index", "0");
                $("#player1-turn").css("top", "0px", "z-index", "0");
                p1Turn = false;
            }
            else {
                $("#player1-turn").css("top", "-50px", "z-index", "0");
                $("#player2-turn").css("top", "0px", "z-index", "0");
                p1Turn = true;
            }
        }

    }
    function moreGames() {
        debugger;
        firstGame = false;
        $('#resetBtn').prop('disabled', false);
        // switch first player
        if (firstPlayer === player1) {
            firstPlayer = player2
        }
        else {
            firstPlayer = player1
        }
        scoreX = [];
        scoreO = [];
        count = 0;
        board = { 1: "", 2: "", 3: "", 4: "", 5: "", 6: "", 7: "", 8: "", 9: "" };

        whoStarts();


    }


    function computerMove() {
        debugger
        var move = winOrBlockChoice('win')[0];
        if (!move) {
            move = winOrBlockChoice('block')[0];
        }
        if (!move) {
            move = doubleThreatChoice('win');
        }
        if (!move) {
            move = doubleThreatChoice('block');
        }
        if (!move) {
            move = firstPlay();
        }
        if (!move) {
            move = playCenter();
        }
        if (!move) {
            move = emptyCorner();
        }
        if (!move) {
            move = emptySide();
        }
        move = (move && board[move]) === '' ? move : false;
        // return move;
        if (move) {
            setTimeout(function () {
                $("#box" + move).text(computer);
                board[move] = computer;
                if (computer === "X") {
                    scoreX.push(move);
                }
                else {
                    scoreO.push(move);
                }
                resultDetermin();
            }, 800)

        }
        count++;
    }
    function winOrBlockChoice(choiceType) {
        // debugger
        if (choiceType === "win") {
            var currentSymbol = computer;
            var opponentSymbol = player1;
        }
        else if (choiceType === "block") {
            var currentSymbol = player1;
            var opponentSymbol = computer;
        }
        else {
            return;
        }
        var moves = [];
        winPaths.forEach(function (winPath) {
            var moveable = true;
            var availableBoxs = [];
            for (var i = 0; i < winPath.length; i++) {
                // debugger
                if (board[winPath[i]] !== currentSymbol) {
                    if (board[winPath[i]] === opponentSymbol) {
                        moveable = false;
                    }
                    else {
                        availableBoxs.push(winPath[i]);
                    }
                }
            }
            if (availableBoxs.length === 1 && moveable) {
                var move = availableBoxs[0];
                moves.push(move);
            }

        })
        return moves;
    }
    function doubleThreatChoice(choiceType) {
        // debugger
        // var board = boarder;
        var move;

        if (choiceType === 'win') {
            var currentSymbol = computer;
            var opponentSymbol = player1;
        } else if (choiceType === 'block') {
            var currentSymbol = player1;
            var opponentSymbol = computer;
        }

        // forced diagonal win on 4th move prevention
        if (board[5] === currentSymbol && count === 3) {
            if ((board[1] === opponentSymbol && board[9] === opponentSymbol) || (board[3] === opponentSymbol && board[7] === opponentSymbol)) {
                // Play an edge to block double threat
                move = emptySide();
            }
        }

        if (!move && board[5] === opponentSymbol && count === 2) {
            move = diagonalSecondAttack();
        }

        if (!move) {
            // clone current board;
            var testBoard = $.extend({}, board);
            for (var i = 1; i <= 9; i++) {

                testBoard = $.extend({}, board);
                if (testBoard[i] === '') {
                    testBoard[i] = currentSymbol;
                    if (winOrBlockChoice(choiceType).length >= 2) {
                        move = i;
                    }
                }
            }
        }
        return move || false;
    }

    function diagonalSecondAttack() {
        // debugger
        var comp = computer;
        var corners = [1, 3, 7, 9];
        for (var i = 0; i < corners.length; i++) {
            if (board[corners[i]] === comp) {
                return 10 - corners[i];
            }
        }
    }

    function firstPlay() {
        // debugger
        var corners = [1, 3, 7, 9];
        var move;
        if (count === 1) {
            // player plays center
            if (board[5] === player1) {
                var cornerNum = Math.floor(Math.random() * 4 + 1);
                move = [1, 3, 7, 9][cornerNum];
            }
            //player plays corner, play opposite corner
            else {
                for (var i = 0; i < corners.length; i++) {
                    if (board[corners[i]] === player1) {
                        move = 5;
                    }
                }
            }
        } else if (count === 0) {
            var cornerNum = Math.floor(Math.random() * corners.length + 1);
            move = corners[cornerNum];
        }
        return move ? move : false;
    }
    function playCenter() {
        // debugger
        if (board[5] === '') {
            return 5;
        }
    }
    function emptyCorner() {

        var corners = [1, 3, 7, 9];
        var move;
        for (var i = 0; i < corners.length; i++) {
            if (board[corners[i]] === '') {
                move = corners[i];
            }
        }
        return move || false;
    }

    function emptySide() {
        // debugger
        var sides = [2, 4, 6, 8];
        for (var i = 0; i < sides.length; i++) {
            if (board[sides[i]] === '') {
                return sides[i];
            }
        }
        return false;
    }



})