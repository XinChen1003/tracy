$(document).ready(function () {
    var skycons = new Skycons({ "color": "pink" });
    function myfun(str) {
        var array = [];
        array = str.split('-');
        return array.join('_');
    }
    if ("geolocation" in navigator) {
        navigator.geolocation.getCurrentPosition(
            function (position) {
                var lat;
                var lon;
                lat = position.coords.latitude;
                lon = position.coords.longitude;
                $.getJSON("https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + lon + "&key=AIzaSyA2DLrLeES9Zn1hks8A8bgqYEF5yCTsUOg", function (data) {
                    $("#location").text(data.results[0].address_components[3].long_name);
                });
                $.getJSON("https://api.darksky.net/forecast/d7cfe5defcaa1aab45b9b5542b403143/" + lat + "," + lon + "?callback=?", function (data) {

                    var test = data.currently.icon.toUpperCase();
                    $("#temp").text(data.currently.temperature);
                    $("#summary").text(data.currently.summary);
                    skycons.add("icon", Skycons[myfun(test)]);
                    skycons.play();

                    $("#myBtn").click(function () {
                        if ($("#temp").text() == data.currently.temperature) {
                            var f = data.currently.temperature;
                            var c = Math.round((f - 32) * 5 / 9 * 100) / 100;
                            $("#temp").text(c);
                            $("#myBtn").text('C');
                        }
                        else {
                            $("#temp").text(data.currently.temperature);
                            $("#myBtn").text('F');
                        }
                    })
                });
            }
        );
    }
    else {
        console.log("Browser doesn't support geolocation!");
    };
})