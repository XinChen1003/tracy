$(document).ready(function () {
    var entry = '';
    var ans = '';
    var current = '';
    var log = '';
    var decimal = true;
    var reset = '';

    function round(val) {
        debugger;
        val = val.toString();
        if (val.indexOf('.') !== -1) {
            var valDeci = val.slice(val.indexOf('.') + 1);
            val = val.slice(0, val.indexOf('.') + 1);
            var i = 0;
            while (valDeci[i] == 0) {
                i++;
            }
            if (i >= 3) {
                valDeci = '';
                return val.slice(0, -1);
            }
            var j = 0;
            if (valDeci.length >= 3) {
                if (valDeci[2] >= 5) {
                    j = 1;
                }
            }

            valDeci = valDeci.slice(0, 2);

            if (valDeci[valDeci.length - 1] == 0) {
                valDeci = valDeci.slice(0, -1);
            }
            if (j === 1) {
                val += valDeci;
                return Number(val) + 0.01;
            }


            return val + valDeci;

        }

        return val;

    }


    $("button").click(function () {
        entry = $(this).attr('value');

        if (reset) {

            if (entry === '/' || entry === '*' || entry === '+' || entry === '-') {

                log = ans;
            }

            else {
                ans = '';
            }
        }
        reset = false;


        if (entry === 'ac' || entry === 'ce' && current === 'noChange') {

            ans = '';
            current = '';
            entry = '';
            log = '';
            $('#history').html('0');
            $('#answer').html('0');
            decimal = true;

        }
        else if (entry === 'ce') {

            log = log.slice(0, -current.length);
            ans = ans.slice(0, -current.length);
            $("#history").html(log);
            current = ans;
            if (log.length === 0 || log === '') {
                $("#history").html('0');
            }
            $('#answer').html('0');
            entry = '';
            decimal = true;

        }

        if (current !== 'noChange') {
            if (isNaN(current) && isNaN(entry) || current === '' && isNaN(entry)) {
                entry = '';
            }
        }

        if (entry === '.' || entry === '0.') {

            if (!decimal) {

                entry = '';
            }
        }

        if (ans.length === 0 && isNaN(entry) && entry !== '.' || ans.length === 0 && entry === '0') {

            entry = '';
            ans = '';
        }


        while (Number(entry) || entry === '0' || current === '.') {


            if (isNaN(current) && entry === '0' && current !== '.') {
                entry = '';
            }
            else if (isNaN(current) && Number(entry) && current !== '.') {
                current = '';
            }
            if (entry === '.') {
                decimal = false;
            }
            if (current === '0.' && isNaN(entry)) {
                entry = '';
            }
            else {
                if (current[current.length - 1] === '.') {
                    current = current.concat(entry);
                }
                else {
                    current += entry;
                }
                ans += entry;
                $("#answer").html(current);

                log += entry;

                $("#history").html(log);
                entry = '';

            }


        }

        if (entry === '.') {

            if (current === '' || isNaN(current[current.length - 1])) {

                current = '0.';
                ans += entry;
                $("#answer").html("0.");
                log += current;
                $("#history").html(log);
            }
            else {
                current = current.concat('.');
                ans = ans.concat('.');
                log = ans;
                $("#answer").html(current);
                $("#history").html(log);
            }
            decimal = false;//no more '.'
            entry = '';
        }
        else if (entry === '/') {
            current = '/';
            ans = round(eval(ans)) + current;
            log += current;
            $("#answer").html('/');
            $("#history").html(log);
            entry = "";
            decimal = true;
        }
        else if (entry === '*') {
            current = '*';
            ans = round(eval(ans)) + current;
            log += current;
            $("#answer").html('X');
            $("#history").html(log);
            entry = "";
            decimal = true;
        }
        else if (entry === '-') {
            current = '-';
            ans = round(eval(ans)) + current;
            log += current;
            $('#history').html(log);
            $('#answer').html('-');
            entry = '';
            decimal = true;
        }
        else if (entry === '+') {
            debugger
            current = '+';
            ans = round(eval(ans)) + current;
            log += current;
            $('#history').html(log);
            $('#answer').html('+');
            entry = '';
            decimal = true;

        }
        else if (entry === '=') {
            debugger;
            if (current[current.length - 1] === '.') {
                entry = '';
            }
            else {
                current = eval(ans).toString();
                ans = round(eval(ans));
                $("#answer").html(ans);
                log += entry + ans;
                $('#history').html(log);
                //log = ans; // this line repeat on if(reset) statement
                entry = '';
                reset = true;
                decimal = true;
            }
            current = 'noChange';
        }

        if (reset) {
            log = '';

        }
        if ($("#answer").text().length > 8 || $("#history").text().length > 22) {
            debugger;
            $("#answer").html('0');
            $("#history").html('Digit Limit Met');
            entry = '';
            ans = '';
            current = '';
            log = '';
            decimal = true;
            reset = '';

        }


    })





})





