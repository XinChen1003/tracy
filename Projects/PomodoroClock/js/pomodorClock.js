$(document).ready(function () {
    var sessionLen = parseInt($("#session").html());
    var breakLen = parseInt($("#break").html());
    var isSession = true;
    var myInterval;
    var isRestart = false;
    var reset = true;
    $("#display").html(sessionLen);
    //session plus button
    $("#session-plus").click(function () {
        if(!reset) return;
        sessionLen++;
        $("#session").html(sessionLen);
        $("#display").html(sessionLen);
    })
    //breakLength plus button 
    $("#break-plus").click(function () {
        if(!reset) return;
        breakLen++;
        $("#break").html(breakLen);
    })
    //session minus button
    $("#session-minus").click(function () {
        if(!reset) return;
        if (sessionLen === 0) return;
        sessionLen--;
        $("#session").html(sessionLen);
        $("#display").html(sessionLen);
    })

    //breakLength minus button
    $("#break-minus").click(function () {
        if(!reset) return;
        if (breakLen === 0) return;
        breakLen--;
        $("#break").html(breakLen);
    })

    // countdown timer 
    function countdown() {
        debugger;
        var duration = sessionLen * 60;
        var minutes, seconds;
        //if(isSession) {
        //$(".content").css("transition", "background-color ")
        //}
        var myFun = function () {
            //debugger;
            minutes = parseInt(duration / 60, 10);
            seconds = parseInt(duration % 60, 10);
            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;
            $("#display").html(minutes + ":" + seconds);
            duration--;
            if (duration < 0) {
                duration = isSession ? breakLen * 60 : sessionLen * 60;
                if (isSession) {
                    isSession = false;
                    $("#title").html("Break");
                }
                else {
                    isSession = true;
                    $("#title").html("Session");
                }
            }
        }
        myInterval = setInterval(myFun, 1000);
    }

    //restart coundown
    function restartCoundown() {
        debugger
        var newLen = $("#display").html();
        newLen = newLen.split(":");
        var minutes = newLen[0];
        var seconds = newLen[1];
        var duration = parseInt(minutes) * 60 + parseInt(seconds);
        var myNewFun = function () {
            minutes = parseInt(duration / 60, 10);
            seconds = parseInt(duration % 60, 10);
            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;
            $("#display").html(minutes + ":" + seconds);
            duration--;
            if (duration < 0) {
                duration = isSession ? breakLen * 60 : sessionLen * 60;
                if (isSession) {
                    isSession = false;
                    $("#title").html("Break");
                }
                else {
                    isSession = true;
                    $("#title").html("Session");
                }
            }
        }
        myInterval = setInterval(myNewFun, 1000);
    }
    // start button
    $("#startBtn").click(function () {
        debugger
        if(isRestart) {
            restartCoundown();
        }
        else{
            countdown();
        } 
        $(this).css("display", "none");
        $("#pauseBtn").css("display", "inline-block");
    })

    //pause button
    $("#pauseBtn").click(function () {
        debugger
        $(this).css("display", "none");
        $("#startBtn").css("display", "inline-block");
        clearInterval(myInterval);
        isRestart = true;
        reset = false;
    })
    // refresh button 
    $("#refreshBtn").click(function () {
        clearInterval(myInterval);
        $("#title").html("Session")
        $("#display").html(sessionLen);
        $("#startBtn").css("display", "inline-block");
        $("#pauseBtn").css("display", "none");
        isRestart = false;
        reset = true;
    })

})

